package com.scc.client.domain.example

import com.scc.client.domain.BaseUseCase
import kotlinx.coroutines.delay

class ExampleUseCase: BaseUseCase<Unit, String>() {

    override suspend fun invoke(param: Unit): Result<String> = kotlin.runCatching {
        delay(1000)
        "Hello from useCase"
    }
}
