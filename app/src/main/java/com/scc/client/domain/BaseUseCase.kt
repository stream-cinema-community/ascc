package com.scc.client.domain

abstract class BaseUseCase<P, R> {
    abstract suspend operator fun invoke(param:P):Result<R>
}
