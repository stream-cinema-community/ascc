package com.scc.client.tools.network

import com.scc.client.tools.Constants.Api.ACCESS_TOKEN
import com.scc.client.tools.Constants.Api.ACCESS_TOKEN_QUERY_PARAM
import okhttp3.Interceptor
import okhttp3.Response

class ScAuthenticationInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequest = chain.request().newBuilder().url(
            chain.request().url.newBuilder()
                .addEncodedQueryParameter(ACCESS_TOKEN_QUERY_PARAM, ACCESS_TOKEN).build()
        ).build()
        return chain.proceed(newRequest)
    }
}
