package com.scc.client.tools

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.databinding.BindingAdapter
import androidx.databinding.BindingConversion
import timber.log.Timber

@BindingConversion
fun visibility(visible: Boolean) = if (visible) View.VISIBLE else View.GONE

fun View.visibility(visible: Boolean) = if (visible) View.VISIBLE else View.GONE

@BindingAdapter("invisible")
fun View.isInvisible(isInvisible: Boolean) {
    if (isInvisible) {
        this.visibility = View.INVISIBLE
    } else {
        this.visibility = View.VISIBLE
    }
}

fun View.showSoftKeyboard() {
    val inputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
    inputMethodManager?.let {
        requestFocus()
        it.showSoftInput(this, 0)
    }
}

fun View.hideSoftKeyboard() {
    val inputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
    inputMethodManager?.hideSoftInputFromWindow(windowToken, 0)
}

@BindingAdapter("app:layout_width")
fun View.setLayoutWidth(width: Float) {
    Timber.d("XYZ: new width = $width")
    val layoutParams = this.layoutParams
    layoutParams.width = width.toInt()
    this.layoutParams = layoutParams
}
