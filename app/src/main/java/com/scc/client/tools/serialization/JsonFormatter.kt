package com.scc.client.tools.serialization

import kotlinx.serialization.json.Json

object JsonFormatter {
    val json = Json(from  = Json) {
        encodeDefaults = true
        ignoreUnknownKeys = true
        isLenient = true
        coerceInputValues = true
    }
}
