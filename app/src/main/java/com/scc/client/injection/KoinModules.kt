package com.scc.client.injection

import android.content.Context
import com.scc.client.data.model.ui.MenuItem
import com.scc.client.data.remote.SccApiManager
import com.scc.client.data.remote.SccApiService
import com.scc.client.domain.example.ExampleUseCase
import com.scc.client.tools.Constants.Api.BASE_URL
import com.scc.client.tools.network.ScAuthenticationInterceptor
import com.scc.client.tools.serialization.JsonFormatter
import com.scc.client.ui.home.HomeViewModel
import com.scc.client.ui.home.HomeViewState
import com.scc.client.ui.navigation.NavigationViewModel
import com.scc.client.ui.navigation.NavigationViewState
import com.scc.client.ui.navigationmenu.MenuItemAdapter
import com.scc.client.ui.navigationmenu.NavigationMenuViewModel
import com.scc.client.ui.navigationmenu.NavigationMenuViewState
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.plugins.DefaultRequest
import io.ktor.client.plugins.cache.HttpCache
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.http.ContentType
import io.ktor.http.contentType
import io.ktor.serialization.kotlinx.json.json
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import timber.log.Timber

val appModule = module {
    single<Context> { this.androidApplication() }
    single { this.androidApplication().resources }
}

val network = module {
    single { JsonFormatter.json }
    single {
        HttpLoggingInterceptor {
            Timber.tag("OkHttp").d(it)
        }.apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }
    single(named("baseHttpClient")) { OkHttpClient.Builder().build() }
    single { ScAuthenticationInterceptor() }
    single(named("scClient")) {
        get<OkHttpClient>(named("baseHttpClient")).newBuilder()
            .addInterceptor(get<ScAuthenticationInterceptor>())
            .addInterceptor(get<HttpLoggingInterceptor>())
            .build()
    }
    single {
        HttpClient(OkHttp) {
            expectSuccess = true
            engine { preconfigured = get(named("scClient")) }
            install(HttpCache)
            install(ContentNegotiation) {
                json(get())
            }
            install(DefaultRequest) {
                url(BASE_URL)
                contentType(ContentType.Application.Json)
            }
        }
    }
    single { SccApiService(get()) }
    single { SccApiManager(get(), get(), get()) }

}

val domain = module {
    factory { ExampleUseCase() }
}

val viewModels = module {
    viewModel { NavigationMenuViewModel(NavigationMenuViewState()) }
    viewModel { NavigationViewModel(NavigationViewState()) }
    viewModel { HomeViewModel(HomeViewState(), get()) }
}

val others = module {
    factory { (focusCallback: (MenuItem) -> Unit, clickCallback: (MenuItem) -> Unit) ->
        MenuItemAdapter(
            focusCallback,
            clickCallback
        )
    }
}
