package com.scc.client.ui.home

import com.scc.client.ui.base.Event

sealed class HomeEvents : Event<HomeViewState>() {
}
