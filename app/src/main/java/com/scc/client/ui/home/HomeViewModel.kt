package com.scc.client.ui.home

import androidx.lifecycle.viewModelScope
import com.scc.client.domain.example.ExampleUseCase
import com.scc.client.ui.base.BaseViewModel
import kotlinx.coroutines.launch
import timber.log.Timber

class HomeViewModel(
    override val viewState: HomeViewState,
    private val exampleUseCase: ExampleUseCase
) : BaseViewModel<HomeViewState>() {

    init {
        viewModelScope.launch {
            exampleUseCase.invoke(Unit)
                .onSuccess { viewState.dynamicText.value = it }
                .onFailure { Timber.e(it) }
        }
    }
}
