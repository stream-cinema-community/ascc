package com.scc.client.ui.continuewatching

import com.scc.client.ui.base.BaseViewModel

class ContinueWatchingViewModel(
    override val viewState: ContinueWatchingViewState
) : BaseViewModel<ContinueWatchingViewState>() {
}
