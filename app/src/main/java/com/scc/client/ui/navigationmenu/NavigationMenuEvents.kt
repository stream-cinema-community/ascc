package com.scc.client.ui.navigationmenu

import androidx.annotation.IdRes
import androidx.annotation.StringRes
import com.scc.client.ui.base.Event

sealed class NavigationMenuEvents : Event<NavigationMenuViewState>()
data class NavigateToNewScreenEvent(@IdRes val id: Int) : NavigationMenuEvents()
data class RestoreFocusEvent(@StringRes val textId: Int) : NavigationMenuEvents()
data class MenuExpansionChanged(val isMenuExpanded: Boolean) : NavigationMenuEvents()
