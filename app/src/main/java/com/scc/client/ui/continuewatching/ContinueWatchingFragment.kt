package com.scc.client.ui.continuewatching

import android.os.Bundle
import android.view.View
import com.scc.client.R
import com.scc.client.databinding.FragmentContinueWatchingBinding
import com.scc.client.ui.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class ContinueWatchingFragment :
    BaseFragment<ContinueWatchingViewModel, ContinueWatchingViewState, FragmentContinueWatchingBinding>() {
    override val layoutResId: Int = R.layout.fragment_continue_watching
    override val viewModel: ContinueWatchingViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}
