package com.scc.client.ui.navigation

import com.scc.client.ui.base.Event

sealed class NavigationEvents : Event<NavigationViewState>()
object BackEvent : NavigationEvents()
object CloseMenuEvent : NavigationEvents()
