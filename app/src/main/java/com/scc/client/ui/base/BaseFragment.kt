package com.scc.client.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.scc.client.BR
import kotlinx.coroutines.launch

abstract class BaseFragment<VM : BaseViewModel<VS>, VS : ViewState, B : ViewDataBinding> :
    Fragment() {


    abstract val viewModel: VM

    abstract val layoutResId: Int
    private val brViewModelVariableId = BR.viewModel
    private val brViewStateVariableId = BR.viewState

    val binding: B
        get() = _binding
            ?: throw IllegalStateException("ViewDataBinding cannot be accessed before onCreateView() method call.")

    private var _binding: B? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return setupBindingView(inflater, container, layoutResId) {
            it.setVariable(brViewModelVariableId, viewModel)
            it.setVariable(brViewStateVariableId, viewModel.viewState)
            it.lifecycleOwner = this.viewLifecycleOwner
            _binding = it
        }
    }

    private fun setupBindingView(
        layoutInflater: LayoutInflater,
        container: ViewGroup?,
        layoutResId: Int,
        set: (B) -> Unit
    ): View {
        val binding =
            DataBindingUtil.inflate<B>(layoutInflater, layoutResId, container, false).also {
                set(it)
            }
        return binding.root
    }

    fun observeEvents(onEvent: (Event<VS>) -> Unit) {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.events.collect {
                onEvent(it)
            }
        }
    }
}
