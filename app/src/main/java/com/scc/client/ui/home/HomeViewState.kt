package com.scc.client.ui.home

import androidx.lifecycle.MutableLiveData
import com.scc.client.ui.base.ViewState

class HomeViewState : ViewState {
    val dynamicText = MutableLiveData("")
}
