package com.scc.client.ui.navigation

import androidx.annotation.IdRes

interface NavigationView {
    fun navigateToDestination(@IdRes destinationId: Int)
    fun navigationMenuExpansionChanged(isExpanded: Boolean)
}
