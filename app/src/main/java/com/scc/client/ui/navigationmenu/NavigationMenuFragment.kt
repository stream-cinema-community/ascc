package com.scc.client.ui.navigationmenu

import android.os.Bundle
import android.view.View
import com.scc.client.R
import com.scc.client.data.model.ui.MenuItem
import com.scc.client.databinding.FragmentNavigationMenuBinding
import com.scc.client.ui.base.BaseFragment
import com.scc.client.ui.navigation.NavigationView
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class NavigationMenuFragment :
    BaseFragment<NavigationMenuViewModel, NavigationMenuViewState, FragmentNavigationMenuBinding>(),
    NavigationMenuView {

    override val layoutResId: Int = R.layout.fragment_navigation_menu
    override val viewModel: NavigationMenuViewModel by viewModel()
    private val onFocusChanged: (MenuItem) -> Unit = {
        viewModel.onItemFocused(it)
    }
    private val onClick: (MenuItem) -> Unit = {
        viewModel.onItemSelected(it)
    }
    private val menuItemAdapter: MenuItemAdapter by inject { parametersOf(onFocusChanged, onClick) }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.root.viewTreeObserver.addOnGlobalFocusChangeListener { _, _ ->
            viewModel.focusChanged(binding.root.hasFocus())
        }
        observeEvents {
            when (it) {
                is NavigateToNewScreenEvent -> {
                    (activity as? NavigationView)?.navigateToDestination(it.id)
                }

                is RestoreFocusEvent -> {
                    binding.root.findViewWithTag<View?>(it.textId)?.also {
                        it.requestFocus()
                    }
                }

                is MenuExpansionChanged -> {
                    (activity as? NavigationView)?.navigationMenuExpansionChanged(it.isMenuExpanded)
                }
            }
        }
        binding.menuRecycler.adapter = menuItemAdapter
        viewModel.viewState.items.observe(viewLifecycleOwner) {
            menuItemAdapter.submitList(it)
        }
    }

    override fun onRestoreSelectedItem() {
        viewModel.onRestoreSelectedItem()
    }
}
