package com.scc.client.ui.navigationmenu

import com.scc.client.data.model.ui.MenuItem
import com.scc.client.ui.base.BaseViewModel

class NavigationMenuViewModel(
    override val viewState: NavigationMenuViewState
) : BaseViewModel<NavigationMenuViewState>() {

    fun onItemFocused(item: MenuItem) {
        if (viewState.isExpanded.value == false) return
        viewState.items.value = viewState.items.value?.map {
            it.copy(isSelected = it.text == item.text)
        }
    }

    fun onItemSelected(item: MenuItem) {
        viewState.selectedItem = item
        item.navigationDest?.also {
            sendEvent(NavigateToNewScreenEvent(it))
        }
    }

    fun focusChanged(isMenuFocused: Boolean) {
        if (viewState.isExpanded.value == false && isMenuFocused) {
            sendEvent(RestoreFocusEvent(viewState.selectedItem.text))
        }
        viewState.isExpanded.value = isMenuFocused
        sendEvent(MenuExpansionChanged(isMenuFocused))
    }

    fun onRestoreSelectedItem() {
        viewState.items.value = viewState.items.value?.map {
            it.copy(isSelected = it.text == viewState.selectedItem.text)
        }
    }
}
