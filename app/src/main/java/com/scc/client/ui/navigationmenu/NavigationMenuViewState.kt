package com.scc.client.ui.navigationmenu

import androidx.lifecycle.MutableLiveData
import com.scc.client.R
import com.scc.client.data.model.ui.MenuItem
import com.scc.client.ui.base.ViewState


class NavigationMenuViewState : ViewState {

    private val defaultItem = MenuItem(
        text = R.string.home_title,
        textIcon = R.string.house,
        navigationDest = R.id.homeFragment,
        isSelected = true
    )
    var selectedItem: MenuItem = defaultItem

    val items = MutableLiveData(
        listOf(
            defaultItem,
            MenuItem(
                text = R.string.continue_watching_title,
                textIcon = R.string.share,
                navigationDest = R.id.continueWatchingFragment
            ),
            MenuItem(
                text = R.string.history_title,
                textIcon = R.string.history
            ),
            MenuItem(
                text = R.string.downloads_title,
                textIcon = R.string.download
            ),
            // TODO add section item
            MenuItem(
                text = R.string.trends_title,
                textIcon = R.string.trends
            ),
            MenuItem(
                text = R.string.thematic_lists_title,
                textIcon = R.string.thematic_lists
            ),
            MenuItem(
                text = R.string.movies_title,
                textIcon = R.string.movies
            ),
            MenuItem(
                text = R.string.tv_shows_title,
                textIcon = R.string.tv_shows
            ),
            MenuItem(
                text = R.string.concerts_title,
                textIcon = R.string.concerts
            ),
            MenuItem(
                text = R.string.anime_title,
                textIcon = R.string.anime
            ),
            MenuItem(
                text = R.string.tv_programs_title,
                textIcon = R.string.tv_programs
            )
        )
    )
    val isExpanded = MutableLiveData(false)
}
