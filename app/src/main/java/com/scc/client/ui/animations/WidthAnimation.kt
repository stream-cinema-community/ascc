package com.scc.client.ui.animations

import android.view.View
import android.view.animation.Animation
import android.view.animation.Transformation

class WidthAnimation(protected val view: View, protected val originalWidth: Int, toWidth: Int) :
    Animation() {
    private var perValue: Float

    init {
        perValue = (toWidth - originalWidth).toFloat()
    }

    override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
        view.layoutParams.width = (originalWidth + perValue * interpolatedTime).toInt()
        view.requestLayout()
    }

    override fun willChangeBounds(): Boolean {
        return true
    }
}
