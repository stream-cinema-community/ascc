package com.scc.client.ui.navigation

import com.scc.client.ui.base.ViewState

class NavigationViewState : ViewState {
    var menuExpansionState: Boolean = false
}
