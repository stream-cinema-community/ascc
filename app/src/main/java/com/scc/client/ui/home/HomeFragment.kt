package com.scc.client.ui.home

import android.os.Bundle
import android.view.View
import com.scc.client.R
import com.scc.client.databinding.FragmentHomeBinding
import com.scc.client.ui.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment<HomeViewModel, HomeViewState, FragmentHomeBinding>() {

    override val layoutResId: Int = R.layout.fragment_home

    override val viewModel: HomeViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}
