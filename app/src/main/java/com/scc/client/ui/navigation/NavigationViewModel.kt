package com.scc.client.ui.navigation

import com.scc.client.ui.base.BaseViewModel

class NavigationViewModel(
    override val viewState: NavigationViewState
) : BaseViewModel<NavigationViewState>() {

    fun onBack() {
        if (viewState.menuExpansionState) {
            sendEvent(CloseMenuEvent)
        } else {
            sendEvent(BackEvent)
        }
    }

    fun menuExpansionStateChanged(isMenuExpanded: Boolean) {
        viewState.menuExpansionState = isMenuExpanded
    }
}
