package com.scc.client.ui.navigationmenu

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.scc.client.R
import com.scc.client.data.model.ui.MenuItem
import com.scc.client.databinding.ItemMenuBinding
import com.scc.client.tools.visibility

class MenuItemAdapter(
    private val onFocusChanged: (MenuItem) -> Unit,
    private val onClick: (item: MenuItem) -> Unit
) : ListAdapter<MenuItem, MenuItemAdapter.MenuItemViewHolder>(DiffUtilCallback) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuItemViewHolder {
        return MenuItemViewHolder(
            ItemMenuBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), onFocusChanged = onFocusChanged, onClick
        )
    }

    override fun onBindViewHolder(holder: MenuItemViewHolder, position: Int, payload: List<Any?>) {
        if (payload.firstOrNull() as Boolean? == true) {
            holder.onSelectedChanged(getItem(position))
        } else {
            holder.onBind(getItem(position))
        }

    }

    override fun onBindViewHolder(holder: MenuItemViewHolder, position: Int) {
        holder.onBind(getItem(position))
    }

    class MenuItemViewHolder(
        private val menuBinding: ItemMenuBinding,
        private val onFocusChanged: (MenuItem) -> Unit,
        private val onClick: (item: MenuItem) -> Unit
    ) : RecyclerView.ViewHolder(menuBinding.root) {
        private val resources = menuBinding.root.context.resources
        fun onBind(item: MenuItem) {
            menuBinding.button.background =
                if (item.isSelected) {
                    ResourcesCompat.getDrawable(resources, R.drawable.menu_button_focused, null)
                } else {
                    null
                }
            menuBinding.button.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    onFocusChanged(item)
                }
            }
            menuBinding.button.setOnClickListener {
                onClick(item)
            }
            if (item.textIcon != null) {
                menuBinding.button.setText(item.textIcon)
            }
            menuBinding.button.tag = item.text
            menuBinding.title.setText(item.text)
            menuBinding.title.setTextColor(
                if (item.isSelected) ResourcesCompat.getColor(
                    resources,
                    R.color.ink_white,
                    null
                ) else ResourcesCompat.getColor(resources, R.color.ink_blue, null)
            )

            menuBinding.title.visibility(item.isExpanded)
            menuBinding.executePendingBindings()
        }

        fun onSelectedChanged(item: MenuItem) {
            menuBinding.button.background =
                if (item.isSelected) {
                    ResourcesCompat.getDrawable(resources, R.drawable.menu_button_focused, null)
                } else {
                    null
                }
        }
    }

    object DiffUtilCallback : DiffUtil.ItemCallback<MenuItem>() {
        override fun areItemsTheSame(oldItem: MenuItem, newItem: MenuItem): Boolean =
            oldItem.text == newItem.text

        override fun areContentsTheSame(oldItem: MenuItem, newItem: MenuItem): Boolean =
            oldItem == newItem

        override fun getChangePayload(oldItem: MenuItem, newItem: MenuItem): Any? {
            return null
        }
    }
}
