package com.scc.client.ui.navigationmenu

interface NavigationMenuView {
    fun onRestoreSelectedItem()
}
