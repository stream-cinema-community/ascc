package com.scc.client.ui.navigation

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.KeyEvent
import androidx.navigation.Navigation
import com.scc.client.R
import com.scc.client.databinding.ActivityNavigationBinding
import com.scc.client.ui.base.BaseActivity
import com.scc.client.ui.navigationmenu.NavigationMenuFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class NavigationActivity :
    BaseActivity<NavigationViewModel, NavigationViewState, ActivityNavigationBinding>(),
    NavigationView {

    override val layoutResId: Int = R.layout.activity_navigation

    private lateinit var navMenuFragment: NavigationMenuFragment
    private val navController by lazy { Navigation.findNavController(this, R.id.nav_host_fragment) }

    override val viewModel: NavigationViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initMenu()
        observeEvents {
            when (it) {
                is BackEvent -> {
                    if (navController.popBackStack().not()) {
                        finish()
                    }
                }

                is CloseMenuEvent -> {
                    navMenuFragment.onRestoreSelectedItem()
                    binding.navHostFragment.requestFocus()
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        binding.navHostFragment.requestFocus()
    }

    private fun initMenu() {
        navMenuFragment = NavigationMenuFragment()
        supportFragmentManager.beginTransaction().replace(R.id.menu_container, navMenuFragment)
            .commitNow()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        Log.i("KEY", keyCode.toString())
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            viewModel.onBack()
            return false
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun navigateToDestination(destinationId: Int) {
        navController.popBackStack()
        navController.navigate(destinationId)
        Handler(Looper.getMainLooper()).postDelayed({
            binding.navHostFragment.requestFocus()
        }, 300)
    }

    override fun navigationMenuExpansionChanged(isExpanded: Boolean) {
        viewModel.menuExpansionStateChanged(isExpanded)
    }
}
