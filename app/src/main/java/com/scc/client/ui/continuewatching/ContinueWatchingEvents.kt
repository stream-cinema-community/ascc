package com.scc.client.ui.continuewatching

import com.scc.client.ui.base.Event

sealed class ContinueWatchingEvents : Event<ContinueWatchingViewState>() {
}
