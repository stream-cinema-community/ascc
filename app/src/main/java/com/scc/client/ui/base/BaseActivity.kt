package com.scc.client.ui.base

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.lifecycleScope
import com.scc.client.BR
import kotlinx.coroutines.launch

abstract class BaseActivity<VM : BaseViewModel<VS>, VS : ViewState, B : ViewDataBinding> :
    FragmentActivity() {

    abstract val viewModel: VM
    abstract val layoutResId: Int
    private val brViewModelVariableId = BR.viewModel
    private val brViewStateVariableId = BR.viewState


    val binding: B
        get() = _binding
            ?: throw IllegalStateException("ViewDataBinding cannot be accessed before onCreate() method call.")

    private var _binding: B? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = setupBindingView(this, layoutResId) {
            it.setVariable(brViewModelVariableId, viewModel)
            it.setVariable(brViewStateVariableId, viewModel.viewState)
            it.lifecycleOwner = this
        }
    }

    private fun setupBindingView(
        fragmentActivity: FragmentActivity,
        layoutResId: Int,
        set: (B) -> Unit
    ): B {
        return DataBindingUtil.setContentView<B>(fragmentActivity, layoutResId).also {
            set(it)
        }
    }

    fun observeEvents(onEvent: (Event<VS>) -> Unit) {
        lifecycleScope.launch {
            viewModel.events.collect {
                onEvent(it)
            }
        }
    }
}
