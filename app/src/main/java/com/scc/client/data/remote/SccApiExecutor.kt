package com.scc.client.data.remote

import android.content.Context
import com.scc.client.R
import io.ktor.client.call.body
import io.ktor.client.plugins.ClientRequestException
import io.ktor.client.plugins.ServerResponseException
import io.ktor.client.statement.HttpResponse
import io.ktor.util.cio.writeChannel
import io.ktor.utils.io.ByteReadChannel
import io.ktor.utils.io.copyAndClose
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import java.io.File
import java.io.IOException

open class SccApiExecutor(val json: Json, val context: Context) {

    protected suspend inline fun <reified T> executeApiCall(apiCall: suspend () -> HttpResponse): T {
        return try {
            apiCall().processResponse()
        } catch (e: ClientRequestException) {
            val errorData =
                e.response.parseError(context.resources.getString(R.string.error_bad_request))
            throw ApiExceptionBadRequest(errorData.first, errorData.second, e)
        } catch (e: ServerResponseException) {
            val errorData =
                e.response.parseError(context.resources.getString(R.string.error_general_message))
            throw ApiExceptionUnknown(errorData.first, errorData.second, e)
        } catch (e: IOException) {
            throw ApiExceptionConnectionError(
                context.resources.getString(R.string.error_connection_message),
                e
            )
        } catch (e: SerializationException) {
            throw ApiExceptionParseError(context.resources.getString(R.string.error_parse), e)
        }
    }

    protected suspend fun executeApiCallWithoutResponse(apiCall: suspend () -> HttpResponse): Unit =
        executeApiCall(apiCall)

    protected suspend fun executeFileDownload(file: File, apiCall: suspend () -> HttpResponse) {
        executeApiCall<ByteReadChannel>(apiCall).copyAndClose(file.writeChannel())
    }

    protected suspend inline fun <reified T> HttpResponse.processResponse(): T {
        return this.body()
    }

    protected suspend inline fun HttpResponse.parseError(defaultMessage: String): Pair<String, ErrorCode> =
        kotlin.runCatching {
            TODO("check for error response format")
        }.getOrDefault(defaultMessage to ErrorCode.UNKNOWN)
}
