package com.scc.client.data.remote

sealed class ApiException(message: String, val errorCode: ErrorCode, cause: Throwable?) : Exception(message, cause)

class ApiExceptionParseError(message: String, cause: Throwable?) :
    ApiException(message, ErrorCode.NONE, cause)

class ApiExceptionConnectionError(message: String, cause: Throwable?) :
    ApiException(message, ErrorCode.NONE, cause)

class ApiExceptionUnAuthorized(message: String, cause: Throwable?) :
    ApiException(message, ErrorCode.NONE, cause)

class ApiExceptionForbidden(message: String, cause: Throwable?) :
    ApiException(message, ErrorCode.NONE, cause)

class ApiExceptionUnknown(message: String, errorCode: ErrorCode, cause: Throwable?) : ApiException(message, errorCode, cause)

class ApiExceptionBadRequest(message: String, errorCode: ErrorCode, cause: Throwable?) :
    ApiException(message, errorCode, cause)

enum class ErrorCode {
    UNKNOWN,
    NONE,
    CREDENTIAL_CHALLENGE_EXPIRED,
    INVALID_EMAIL_FORMAT,
    CURRENT_PASSWORD_MISMATCH,
    INVALID_OBJECT,
    MISSING_CODE_ERROR,
    CODE_CHALLENGE_FAILURE_ERROR,
    CUSTOMER_SEARCH_FAILED
}
