package com.scc.client.data.model.ui

import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.annotation.StringRes

data class MenuItem(
    @StringRes val text: Int,
    @StringRes val textIcon: Int?,
    @DrawableRes val drawableIcon: Int? = null,
    @IdRes val navigationDest: Int? = null,
    val isSelected: Boolean = false,
    val isExpanded: Boolean = false
)
