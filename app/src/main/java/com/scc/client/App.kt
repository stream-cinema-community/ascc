package com.scc.client

import android.app.Application
import com.scc.client.injection.appModule
import com.scc.client.injection.domain
import com.scc.client.injection.network
import com.scc.client.injection.others
import com.scc.client.injection.viewModels
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        startKoin {
            androidContext(this@App)
            modules(appModule, domain, network, viewModels, others)
        }
    }
}
